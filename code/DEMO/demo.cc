#include <iostream>
#include <exception>
#include <cassert>
#include <cmath>
#include <iomanip>
#include <vector>
#include <cfloat>
#include <unistd.h>

#include "display.hh"
#include "vector3.hh"
#include "point3.hh"
#include "color.hh"
#include "hit.hh"
#include "lightray.hh"
#include "shape.hh"
#include "ball.hh"
#include "cube.hh"
#include "plane.hh"
#include "screen.hh"
#include "optic.hh"

using namespace std;

const unsigned width = 800;
const unsigned height = 600;
const Point3 origin = Point3(0, 0, 0);

void demo_1()
{

	double position_x_screen = -(static_cast<double>(width))/2;
	double position_y_screen = -(static_cast<double>(height))/2;
	const Point3 screen_position(position_x_screen, position_y_screen, 100);
	Screen screen(origin, screen_position, width, height);

	// Define balls
	Point3 ball1_position(-150, -75, 200);
	Ball ball1(ball1_position, 100, Color::Red);
	Point3 ball3_position(150, 75, 200);
	Ball ball3(ball3_position, 100, Color::Blue);

	vector<Shape*> shapes = Shape::get_shapes();

	for (unsigned x = 0; x < width; ++x)
	{
		for (unsigned y = 0; y < height; ++y)
		{
			Lightray ray = screen.make_ray(x, y);
			Shape* closest_shape = first_shape_hit(ray, shapes);
			if (closest_shape != NULL)
			{
				screen.set_pixel(x, y, Color::White);
			}

		}
	}
	screen.show();
}

void demo_2()
{

	double position_x_screen = -(static_cast<double>(width))/2;
	double position_y_screen = -(static_cast<double>(height))/2;
	const Point3 screen_position(position_x_screen, position_y_screen, 100);
	Screen screen(origin, screen_position, width, height);

	// Define balls
	Point3 ball1_position(-150, -75, 200);
	Ball ball1(ball1_position, 100, Color::Red);
	Point3 ball3_position(150, 75, 200);
	Ball ball3(ball3_position, 100, Color::Blue);

	vector<Shape*> shapes = Shape::get_shapes();

	for (unsigned x = 0; x < width; ++x)
	{
		for (unsigned y = 0; y < height; ++y)
		{
			Lightray ray = screen.make_ray(x, y);
			Shape* closest_shape = first_shape_hit(ray, shapes);
			if (closest_shape != NULL)
			{
				Color shape_color= (closest_shape->get_color());
				screen.set_pixel(x, y, shape_color);
			}

		}
	}
	screen.show();

}

void demo_3()
{

	double position_x_screen = -(static_cast<double>(width))/2;
	double position_y_screen = -(static_cast<double>(height))/2;
	const Point3 screen_position(position_x_screen, position_y_screen, 100);
	Screen screen(origin, screen_position, width, height);

	// Define balls
	Point3 ball1_position(0, -75, 200);
	Ball ball1(ball1_position, 100, Color::Red);
	Point3 ball2_position(0, 75, 200);
	Ball ball2(ball2_position, 100, Color::Blue);
	Point3 ball3_position(150, 75, 200);
	Ball ball3(ball3_position, 100, Color::Green);

	vector<Shape*> shapes = Shape::get_shapes();

	for (unsigned x = 0; x < width; ++x)
	{
		for (unsigned y = 0; y < height; ++y)
		{
			Lightray ray = screen.make_ray(x, y);
			Shape* closest_shape = first_shape_hit(ray, shapes);
			if (closest_shape != NULL)
			{
				Color shape_color = (closest_shape->get_color());
				screen.set_pixel(x, y, shape_color);
			}

		}
	}
	screen.show();

}

void demo_4()
{

	double position_x_screen = -(static_cast<double>(width))/2;
	double position_y_screen = -(static_cast<double>(height))/2;
	const Point3 screen_position(position_x_screen, position_y_screen, 100);
	Screen screen(origin, screen_position, width, height);

	// Define balls
	Point3 ball1_position(0, -75, 200);
	Ball ball1(ball1_position, 100, Color::Red);
	Point3 ball2_position(0, 75, 200);
	Ball ball2(ball2_position, 100, Color::Blue);
	Point3 ball3_position(150, 75, 200);
	Ball ball3(ball3_position, 100, Color::Green);

	// Define planes
	Point3 plane_position(0, 0, 300);
	Vector3 plane_width(1000, 0, 0);
	Vector3 plane_height(0, 1000, 0);

	Plane plane(plane_position, plane_width, plane_height, Color::Magenta);


	///////////////


	Point3 plane_position_wouah(250, -200, 300);
	Vector3 plane_width_wouah(20, 350, 0);
	Vector3 plane_height_wouah(0, 0, 200);

	Plane plane_wouah(plane_position_wouah, plane_width_wouah,
			plane_height_wouah, Color(20,200,80));

	Point3 ball_wouah_position(250, -200, 300);
	Ball ball_wouah(ball_wouah_position, 90, Color(150,150,150));


	//////////////

	vector<Shape*> shapes = Shape::get_shapes();

	for (unsigned x = 0; x < width; ++x)
	{
		for (unsigned y = 0; y < height; ++y)
		{
			Lightray ray = screen.make_ray(x, y);
			Shape* closest_shape = first_shape_hit(ray, shapes);
			if (closest_shape != NULL)
			{
				Color shape_color= (closest_shape->get_color());
				screen.set_pixel(x, y, shape_color);
			}

		}
	}
	screen.show();
}

void demo_5()
{
	double position_x_screen = -(static_cast<double>(width))/2;
	double position_y_screen = -(static_cast<double>(height))/2;

	const Point3 screen_position
		(position_x_screen, position_y_screen, 100);

	Screen screen(origin, screen_position, width, height);

	// Define balls
	Point3 ball3_position(0, 75, 300);
	Ball ball3(ball3_position, 100, Color::Cyan);

	// Define a light source
	Point3 ball_sun_position(-180, -120, 150);
	Color sun_color(243,159,24);
	Ball sun (ball_sun_position, 20, sun_color, true, 1,0.8,0.5);

	Point3 plane_position(125, -85, 200);
	Vector3 plane_width(0, 300, 0);
	Vector3 plane_height(0, 0, 200);

	Plane plane(plane_position, plane_width, plane_height,
			Color::Red, false, 0.5, 0.5, 0.5);

	Point3 ball2_position(120, -80, 200);
	Ball ball2(ball2_position, 90, Color::White, false, 1, 0, 0);

	vector<Shape*> shapes = Shape::get_shapes();

	for (unsigned x = 0; x < width; ++x)
	{
		for (unsigned y = 0; y < height; ++y)
		{
			Lightray ray = screen.make_ray(x, y);
			Shape* closest_shape = first_shape_hit(ray, shapes);
			if (closest_shape != NULL)
			{
				Hit interception =
					closest_shape->check_intersection(ray);
				if (colors_received_by_all_sources
						(interception, shapes).size() != 0)
				{
					Color shape_color =
						(closest_shape->get_color());
					screen.set_pixel(x, y, shape_color);
				}
			}

		}
	}
	screen.show();
}

void demo_6()
{
	double position_x_screen = -(static_cast<double>(width))/2;
	double position_y_screen = -(static_cast<double>(height))/2;

	const Point3 screen_position
		(position_x_screen, position_y_screen, 100);

	Screen screen(origin, screen_position, width, height);

	// Define balls
	Point3 ball3_position(0, 75, 300);
	Ball ball3(ball3_position, 100, Color::Cyan);

	// Define a light source
	Point3 ball_sun_position(-180, -120, 150);
	Color sun_color(243,159,24);
	Ball sun (ball_sun_position, 20, sun_color, true, 1,0.8,0.5);

	Point3 plane_position(125, -85, 200);
	Vector3 plane_width(0, 300, 0);
	Vector3 plane_height(0, 0, 200);

	Plane plane(plane_position, plane_width, plane_height,
			Color::Red, false, 0.5, 0.5, 0.5);

	Point3 ball2_position(120, -80, 200);
	Ball ball2(ball2_position, 90, Color::White, false, 1, 0, 0);

	vector<Shape*> shapes = Shape::get_shapes();

	for (unsigned x = 0; x < width; ++x)
	{
		for (unsigned y = 0; y < height; ++y)
		{
			Lightray ray = screen.make_ray(x, y);
			Color shape_color = simple_rebound(5, shapes, ray);
			screen.set_pixel(x, y, shape_color);
		}
	}
	screen.show();
}

void demo_7()
{
	double position_x_screen = -(static_cast<double>(width))/2;
	double position_y_screen = -(static_cast<double>(height))/2;

	const Point3 screen_position
		(position_x_screen, position_y_screen, 100);

	Screen screen(origin, screen_position, width, height);

	// Define balls
	Point3 ball3_position(0, 75, 300);
	Ball ball3(ball3_position, 100, Color::Cyan);

	// Define a light source
	Point3 ball_sun_position(-180, -120, 150);
	Color sun_color(243,159,24);
	Ball sun (ball_sun_position, 20, sun_color, true, 1,0.8,0.5);

	Point3 ball_sun2_position(100, -120, 60);
	Ball sun2 (ball_sun2_position, 2, Color::Cyan, true, 1,0.8,0.5);

	Point3 plane_position(125, -85, 200);
	Vector3 plane_width(0, 300, 0);
	Vector3 plane_height(0, 0, 200);

	Plane plane(plane_position, plane_width, plane_height,
		 	Color::Red, false, 0.5, 0.5, 0.5);

	Point3 ball2_position(120, -80, 200);
	Ball ball2(ball2_position, 90, Color::White, false, 1, 0, 0);

	vector<Shape*> shapes = Shape::get_shapes();

	for (unsigned x = 0; x < width; ++x)
	{
		for (unsigned y = 0; y < height; ++y)
		{
			Lightray ray = screen.make_ray(x, y);
			Color shape_color = simple_rebound(5, shapes, ray);
			screen.set_pixel(x, y, shape_color);
		}
	}

	screen.show();
}

void demo_8()
{
	double position_x_screen = -(static_cast<double>(width))/2;
	double position_y_screen = -(static_cast<double>(height))/2;

	const Point3 screen_position
		(position_x_screen, position_y_screen, 100);

	Screen screen(origin, screen_position, width, height);

	// Define balls
	Point3 ball3_position(0, 75, 300);
	Ball ball3(ball3_position, 100, Color::Cyan, false, 1, 0, 1);

	Point3 ball2_position(120, -80, 200);
	Ball ball2(ball2_position, 90, Color::White, false, 0, 1, 0.4);

	// Define light sources
	Point3 ball_sun_position(-180, -120, 150);
	Color sun_color(243,159,24);
	Ball sun (ball_sun_position, 20, sun_color, true, 1,0.8,0.5);

	Point3 plane_position(125, -85, 200);
	Vector3 plane_width(0, 300, 0);
	Vector3 plane_height(0, 0, 200);

	Plane plane(plane_position, plane_width, plane_height,
		 	Color::Red, false, 0.5, 0.5, 0.5);

	vector<Shape*> shapes = Shape::get_shapes();

	for (unsigned x = 0; x < width; ++x)
	{
		for (unsigned y = 0; y < height; ++y)
		{
			Lightray ray = screen.make_ray(x, y);
			Color shape_color = simple_rebound(5, shapes, ray);
			screen.set_pixel(x, y, shape_color);
		}
	}

	screen.show();
}

void demo_9()
{
	double position_x_screen = -(static_cast<double>(width))/2;
	double position_y_screen = -(static_cast<double>(height))/2;

	const Point3 screen_position
		(position_x_screen, position_y_screen, 100);

	Screen screen(origin, screen_position, width, height);

	// Define balls
	Point3 ball3_position(0, 75, 300);
	Ball ball3(ball3_position, 100, Color::Cyan, false, 1, 0, 1);

	Point3 ball2_position(120, -80, 200);
	Ball ball2(ball2_position, 90, Color::White, false, 0, 1, 0.4);

	// Define light sources
	Point3 ball_sun_position(-180, -120, 150);
	Color sun_color(243,159,24);
	Ball sun (ball_sun_position, 20, sun_color, true, 1,0.8,0.5);

	// Define a plane
	Point3 plane_position(125, -85, 200);
	Vector3 plane_width(0, 300, 0);
	Vector3 plane_height(0, 0, 200);

	Plane plane(plane_position, plane_width, plane_height,
			Color::Red, false, 0.5, 0.5, 0.5);

	// Define cubes

	Point3 cube2_position(-120, 120, 150);
	Vector3 cube2_width(100, 0, 0);
	Vector3 cube2_height(0, 100, 0);
	Vector3 cube2_depth(0, 0, 100);
	Cube cube2(cube2_position, cube2_width, cube2_height, cube2_depth,
		 	Color(100,100,100), false, 1, 0, 0.5);

	vector<Shape*> shapes = Shape::get_shapes();

	for (unsigned x = 0; x < width; ++x)
	{
		for (unsigned y = 0; y < height; ++y)
		{
			Lightray ray = screen.make_ray(x, y);
			Color shape_color = scatter_rebound(3, shapes, ray);
			screen.set_pixel(x, y, shape_color);
		}
	}

	screen.show();
}

void demo_10()
{
	double position_x_screen = -(static_cast<double>(width))/2;
	double position_y_screen = -(static_cast<double>(height))/2;

	const Point3 screen_position
		(position_x_screen, position_y_screen, 100);

	Screen screen(origin, screen_position, width, height);

	// Define balls
	Point3 ball3_position(0, 75, 300);
	Ball ball3(ball3_position, 100, Color::Cyan, false, 1, 0, 1);

	Point3 ball2_position(120, -80, 200);
	Ball ball2(ball2_position, 90, Color::White, false, 0, 1, 0.4);

	// Define light sources
	Point3 ball_sun_position(-180, -120, 150);
	Color sun_color(243,159,24);
	Ball sun (ball_sun_position, 20, sun_color, true, 1,0.8,0.5);

	// Define a plane
	Point3 plane_position(125, -85, 200);
	Vector3 plane_width(0, 300, 0);
	Vector3 plane_height(0, 0, 200);

	Plane plane(plane_position, plane_width, plane_height, Color::Red,
			false, 0.5, 0.5, 0.5);

	// Define cubes
	Point3 cube_position(0, 0, 100);
	Vector3 cube_width(800, 0, 0);
	Vector3 cube_height(0, 800, 0);
	Vector3 cube_depth(0, 0, 800);
	Cube cube(cube_position, cube_width, cube_height, cube_depth,
		 	Color::White, false, 1, 1, 0);

	Point3 cube2_position(-120, 120, 150);
	Vector3 cube2_width(100, 0, 0);
	Vector3 cube2_height(0, 100, 0);
	Vector3 cube2_depth(0, 0, 100);
	Cube cube2(cube2_position, cube2_width, cube2_height, cube2_depth,
			Color(100,100,100), false, 1, 0, 0.5);

	vector<Shape*> shapes = Shape::get_shapes();

	for (unsigned x = 0; x < width; ++x)
	{
		for (unsigned y = 0; y < height; ++y)
		{
			Lightray ray = screen.make_ray(x, y);
			Color shape_color = simple_rebound(5, shapes, ray);
			screen.set_pixel(x, y, shape_color);
		}
	}

	screen.show();
}

int main()
{
	demo_1();

	demo_2();

	demo_3();

	demo_4();

	demo_5();

	demo_6();

	demo_7();

	demo_8();

	demo_9();

	demo_10();
}
