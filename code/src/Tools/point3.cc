#include "point3.hh"

/**
 * Create a 3D Point.
 *
 * @param double x, the X coordinate.
 * @param double y, the Y coordinate.
 * @param double z, the Z coordinate.
 */
Point3::Point3(double x, double y, double z)
	: x(x), y(y), z(z) {
	}

/**
 * Get the X coordinate.
 *
 * @return double, X coordinate.
 */
double Point3::get_x() const {
	return x;
}

/**
 * Get the Y coordinate.
 *
 * @return double, Y coordinate.
 */
double Point3::get_y() const {
	return y;
}

/**
 * Get the Z coordinate.
 *
 * @return double, Z coordinate.
 */
double Point3::get_z() const {
	return z;
}

/**
 * Plus operator between a 3D point and a 3D vector.
 *
 * @param Point3 p, a point.
 * @param Vector3 v, the vector to add to the point.
 *
 * @return Point3, the resulted 3D point.
 */
Point3 operator +(const Point3& p, const Vector3& v) {
	return Point3(
			p.get_x() + v.get_x(),
			p.get_y() + v.get_y(),
			p.get_z() + v.get_z()
			);
}

/**
 * Minus operator between a 3D point and a 3D vector.
 *
 * @param Point3 p, a point.
 * @param Vector3 v, the vector.
 *
 * @return Point3, the resulted 3D point.
 */
Point3 operator -(const Point3& p, const Vector3& v) {
	return Point3(
			p.get_x() - v.get_x(),
			p.get_y() - v.get_y(),
			p.get_z() - v.get_z()
			);
}

/**
 * Minus operator between two 3D points, creating the corresponding
 * 3D vector.
 *
 * @param Point3 p, a point.
 * @param Vector3 v, the vector.
 *
 * @return Point3, the resulted 3D vector.
 */
Vector3 operator -(const Point3& p1, const Point3& p2) {
	return Vector3(
			p1.get_x() - p2.get_x(),
			p1.get_y() - p2.get_y(),
			p1.get_z() - p2.get_z()
			);
}

/**
 * Multiplication operator between two 3D points.
 *
 * @param Point3 p1, first point.
 * @param Point3 p2, second point.
 *
 * @return Point3, the resulted 3D point.
 */
Point3 operator *(const Point3& p1, const Point3& p2) {
	return Point3 (
			p1.get_x() * p2.get_x(),
			p1.get_y() * p2.get_y(),
			p1.get_z() * p2.get_z()
			);
}

/**
 * Compute the square distance between two 3D points.
 *
 * @param Point3 p1, first point.
 * @param Point3 p2, second point.
 *
 * @return double, the square distance between p1 and p2.
 */
double squared_distance(const Point3& p1, const Point3& p2) {
	return
		(p1.get_x() - p2.get_x()) * (p1.get_x() - p2.get_x()) +
		(p1.get_y() - p2.get_y()) * (p1.get_y() - p2.get_y()) +
		(p1.get_z() - p2.get_z()) * (p1.get_z() - p2.get_z())
		;
}

/**
 * Overload the operator << to take charge of 3D points.
 *
 * @param ostream s, the current stream to use.
 * @param Point3 p, the 3D point.
 *
 * @return ostream, the resulted output stream.
 */
std::ostream& operator <<(std::ostream& s, const Point3& p) {
	double x = p.get_x();
	double y = p.get_y();
	double z = p.get_z();
	return s << "[" << x << ", " << y << ", " << z << "]";
}
