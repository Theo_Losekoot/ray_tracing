#include "vector3.hh"

/**
 * Creating a 3D vector.
 *
 * @param double x, X coordinate.
 * @param double y, Y coordinate.
 * @param double z, Z coordinate.
 */
Vector3::Vector3(double x, double y, double z)
	: x(x), y(y), z(z) {
	}

/**
 * Get the X coordinate of the current 3D vector.
 *
 * @return double, the X coordinate.
 */
double Vector3::get_x() const {
	return x;
}

/**
 * Get the Y coordinate of the current 3D vector.
 *
 * @return double, the Y coordinate.
 */
double Vector3::get_y() const {
	return y;
}

/**
 * Get the Z coordinate of the current 3D vector.
 *
 * @return double, the Z coordinate.
 */
double Vector3::get_z() const {
	return z;
}

/**
 * Compute the vector norme.
 *
 * @param double, vector's length.
 */
double Vector3::length() const {
	return sqrt(x * x + y * y + z * z);
}

/**
 * Equality operator of two 3D vectors. Two vectors are equals if all
 * their coordinates are equals.
 *
 * @param Vector3 v, a 3D vector to compare with.
 *
 * @return bool, true if the coordinates of each vectors are equals,
 * false else.
 */
bool Vector3::operator ==(const Vector3& v) const {
	return x == v.get_x()
		&& y == v.get_y()
		&& z == v.get_z();
}

/**
 * Multiplication operator of a 3D vector by a scalar.
 *
 * @param double t, a scalar.
 * @param Vector3 v, a 3D vector.
 *
 * @return Vector3, the resulted 3D vector.
 */
Vector3 operator *(double t, const Vector3& v) {
	double x = t * v.get_x();
	double y = t * v.get_y();
	double z = t * v.get_z();
	return Vector3(x, y, z);
}

/**
 * Division operator of a 3D vector by a scalar.
 *
 * @param Vector3 v, a 3D vector.
 * @param double t, a scalar.
 *
 * @return Vector3, the resulted 3D vector.
 */
Vector3 operator /(const Vector3& v, double t) {
	return (1/t) * v;
}

/**
 * Plus operator of two 3D vectors.
 *
 * @param Vector3 u, a 3D vector.
 * @param Vector3 v, a 3D vector.
 *
 * @return Vector3, the resulted 3D vector whose coordinates are equals
 * to the sum of u and v coordinates.
 */
Vector3 operator +(const Vector3& u, const Vector3& v) {
	double x = u.get_x() + v.get_x();
	double y = u.get_y() + v.get_y();
	double z = u.get_z() + v.get_z();
	return Vector3(x, y, z);
}

/**
 * Minus operator of two 3D vectors.
 *
 * @param Vector3 u, a 3D vector.
 * @param Vector3 v, a 3D vector.
 *
 * @return Vector3, the resulted 3D.
 */
Vector3 operator -(Vector3& u, const Vector3& v) {
	return u + (-v);
}

/**
 * Inverse each coordinates of a 3D vector.
 *
 * @param Vector3 v, a vector to inverse.
 *
 * @return Vector3, the inverse 3D vector.
 */
Vector3 operator -(const Vector3& v) {
	double a = -v.get_x();
	double b = -v.get_y();
	double c = -v.get_z();
	return Vector3(a, b, c);
}

/**
 * Scalar product between two 3D vectors.
 *
 * @param Vector3 v1, a 3D vector.
 * @param Vector3 v2, a 3D vector.
 *
 * @return double, the scalar product of v1 and v2.
 */
double dot(const Vector3& v1, const Vector3& v2) {
	double a1 = v1.get_x();
	double a2 = v2.get_x();
	double b1 = v1.get_y();
	double b2 = v2.get_y();
	double c1 = v1.get_z();
	double c2 = v2.get_z();
	return (a1 * a2) + (b1 * b2) + (c1 * c2);
}

/**
 * Cross product of two points.
 *
 * @param Vector3 v1, a 3D vector.
 * @param Vector3 v2, a 3D vector.
 */
Vector3 cross(const Vector3& v1, const Vector3& v2) {
	double a1 = v1.get_x();
	double a2 = v2.get_x();
	double b1 = v1.get_y();
	double b2 = v2.get_y();
	double c1 = v1.get_z();
	double c2 = v2.get_z();

	double aa = (b1 * c2) - (c1 * b2);
	double bb = (c1 * a2) - (a1 * c2);
	double cc = (a1 * b2) - (b1 * a2);

	return Vector3(aa, bb, cc);
}

/**
 * Normalize a 3D vector.
 *
 * @param Vector3 v, a 3D vector to normalize.
 *
 * @return Vector3, normalized 3D vector.
 */
Vector3 normalize(const Vector3& v) {
	double vl = v.length();
	assert(vl > 0);
	return v / vl;
}

/**
 * Overload the operator << to take charge of 3D vectors.
 *
 * @param ostream s, the current stream to use.
 * @param Vector3 p, the 3D vector.
 *
 * @return ostream, the resulted output stream.
 */
std::ostream& operator <<(std::ostream& s, const Vector3& v) {
	double x = v.get_x();
	double y = v.get_y();
	double z = v.get_z();
	return s << "(" << x << ", " << y << ", " << z << ")";
}

/**
 * Compute the cosinus of the angle formed by two vectors.
 *
 * @param Vector3 reference, the reference vector.
 * @param Vector3 ray, a vector.
 *
 * @return double, cosinus of the angle.
 */
double cosinus (const Vector3& reference, const Vector3& ray)
{
	// Normalize vectors
	Vector3 ray_normal = normalize(ray);
	Vector3 ref_normal = normalize(reference);

	// Compute cosinus as coefficient
	double coefficient = dot(ref_normal, ray_normal);
	coefficient = std::abs(coefficient);

	double precision = 0.0001;

	assert(coefficient >= -precision && coefficient <= 1 + precision );

	return coefficient;
}
