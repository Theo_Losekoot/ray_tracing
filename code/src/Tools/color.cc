#include "color.hh"

// Set of predefined colors
Color Color::White(255, 255, 255);
Color Color::Black(0, 0, 0);
Color Color::Red(255, 0, 0);
Color Color::Green(0, 255, 0);
Color Color::Blue(0, 0, 255);
Color Color::Yellow(255, 255, 0);
Color Color::Magenta(255, 0, 255);
Color Color::Cyan(0, 255, 255);
Color Color::Grey(127, 127, 127);

/**
 * Create a color in rgb format.
 * 
 * @param int r, red proportion.
 * @warning r must be set between 0 and 255.
 * @param int g, green proportion.
 * @warning g must be set between 0 and 255.
 * @param int b, blue proportion.
 * @warning b must be set between 0 and 255.
 */
Color::Color(int r, int g, int b) :
	red(r), green(g), blue(b)
{
	assert(0 <= red && red <= 255);
	assert(0 <= green && green <= 255);
	assert(0 <= blue && blue <= 255);
}

/**
 * Get the red component of the color.
 * 
 * @return int, the red component.
 */
int Color::get_red() const
{
	return red;
}

/**
 * Get the green component of the color.
 * 
 * @return int, the green component.
 */
int Color::get_green() const
{
	return green;
}

/**
 * Get the blue component of the color.
 * 
 * @return int, the blue component.
 */
int Color::get_blue() const
{
	return blue;
}

/**
 * Equality operator between two colors. Two colors are equals if all
 * their components (r, g, b) are equals.
 * 
 * @param Color c, a color to compare with.
 * 
 * @return bool, true if the colors are equals, false else.
 */
bool Color::operator == (const Color& c) const
{
	return	red == c.red
		&& green == c.green
		&& blue == c.blue;
}

/**
 * Plus operator between of colors.
 * 
 * @param Color c, the color to add.
 * 
 * @return Color, a color corresponding to the addition of the 
 * two colors.
 */
Color Color::operator + (const Color& c) const
{
	int r_add = red + c.red;
	int r = (r_add <= 255) ? r_add : 255;
	int g_add = green + c.green;
	int g = (g_add <= 255) ? g_add : 255;
	int b_add = blue + c.blue;
	int b = (b_add <= 255) ? b_add : 255;
	return Color(r, g, b);
}

/**
 * Multiplication operator of a color by a scalar.
 * 
 * @param double d, the scala.
 * 
 * @return Color, a color corresponding to the main color whose 
 * components are multiply by the scalar d.
 */
Color Color::operator *(const double d) const
{
	int r_times = red * d;
	int r = (r_times <= 255) ? r_times : 255;
	r = (r >= 0) ? r : 0;
	int g_times = green * d;
	int g = (g_times <= 255) ? g_times : 255;
	g = (g >= 0) ? g : 0;
	int b_times = blue * d;
	int b = (b_times <= 255) ? b_times : 255;
	b = (b >= 0) ? b : 0;
	return Color(r, g, b);
}

/**
 * Division operator of a color by a scalar.
 * 
 * @param double d, the scala.
 * 
 * @return Color, a color corresponding to the main color whose 
 * components are divide by the scalar d.
 */
Color Color::operator /(const double d) const
{
	return (*this) * (1/d);
}

/**
 * Compute the resulted color of the absorption of the color c2 by the
 * color c1.
 * 
 * @param Color c1, absorbing color.
 * @param Color c2, absorbed color.
 * 
 * @return Color, the resulted color.
 */
Color Color::absorption (const Color& c1, const Color& c2)
{
	int r = (c1.red * c2.red) / 255;
	int g = (c1.green * c2.green) / 255;
	int b = (c1.blue * c2.blue) / 255;
	return Color(r, g, b);
}

sf::Color Color::to_SFMLColor() const
{
	return sf::Color(static_cast<sf::Uint8>(red),
			static_cast<sf::Uint8>(green),
			static_cast<sf::Uint8>(blue));
}

/**
 * Overload the operator << to take charge of colors.
 * 
 * @param ostream s, the current stream to use.
 * @param Color c, the color.
 * 
 * @return ostream, the resulted output stream.
 */
std::ostream& operator <<(std::ostream& s, const Color& c)
{
	int r = c.get_red();
	int g = c.get_green();
	int b = c.get_blue();
	return s << "Color(" << r << ", " << g << ", " << b << ")";
}

/**
 * Mix two colors according to coefficient (compute the average color).
 * 
 * @param vector<pair<Color, double>> colors, list of couple 
 * representing colors and their coefficient.
 * 
 * @result Color, the average color.
 */
Color mix_colors(const std::vector< std::pair<Color, double> >& colors)
{
	double new_red = 0;
	double new_green = 0;
	double new_blue = 0;

	double coeff = 0;

	for(std::pair<Color, double> p : colors)
	{
		Color temp_color = p.first;
		double temp_coeff = p.second;
		new_red += temp_coeff * temp_color.get_red();
		new_green += temp_coeff * temp_color.get_green();
		new_blue += temp_coeff * temp_color.get_blue();

		coeff += temp_coeff;
	}

	new_red /= coeff;
	new_green /= coeff;
	new_blue /= coeff;

	int red_color = (int) new_red;
	int green_color = (int) new_red;
	int blue_color = (int) new_red;

	return Color(red_color, green_color, blue_color);
}
