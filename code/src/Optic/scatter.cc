#include "scatter.hh"

using namespace std;

/**
 * Normalize coefficient in order that the sum of all coeffs of
 * secondary vects is 1.
 *
 * @param vector<pair<Lightray, double>> vect, couples of light rays
 * with their coefficient.
 * @param double sum_coeff, the sum of light ray's coefficients.
 */
void normalize_coeffs(vector< pair<Lightray, double> >& vect, double sum_coeff)
{
	// make it so that the sum of all coeffs of secondary vects is 1
	for(pair<Lightray, double> iter : vect)
	{
		iter.second = iter.second/sum_coeff;
	}
}

/**
 * Takes two non-zero vectors, returns a normalize vector perpendicular
 * to the two parameters.
 *
 * @param Vector3 main_ray, a 3D vector.
 * @param Vector3 normal, a 3D vector.
 * @warning main_ray and normal are non-zero.
 *
 * @return Vector3, the affore mentioned vector.
 */
Vector3 get_an_orthogonal_vect(Vector3 main_ray, Vector3 normal)
{
	if(cross(main_ray, normal).length() < Shape::PRECISION)
	{
		double nudge = Shape::PRECISION * 4;
		main_ray = main_ray + Vector3(nudge, nudge, nudge);
	}

	Vector3 u = normalize(cross(main_ray, normal));

	return u;
}

/**
 * Compute a coefficient depending on the cone opening.
 *
 * @param double fraction, the cone opening.
 *
 * @return double, the coefficient.
 */
double power_scattered_ray(double fraction)
{
	double coeff = cos(atan(fraction));
	return coeff;
}

/**
 * Updates the cone radius.
 *
 * @param double base_length_modif, maximal cone radius.
 * @param int k, indicates the desired cone radius.
 * @param int nbr_circles, number of circles used to create the cone.
 *
 * @return double, new cone's radius.
 */
	double update_length_modif
(double base_length_modif, int k, int nbr_circles)
{
	return base_length_modif * k / nbr_circles;
}

/**
 * Create a cone of light rays surrounding the main ray.
 *
 * @param Lightray main_ray_ray, main ray.
 * @param Hit hit, starting impact point.
 * @param double cone_opening, maximal cone opening.
 * @param int nbr_rays, number of rays per circle.
 * @param int nbr_circles, number of circles. The cone is built on
 * concentric circles.
 *
 * @return vector<pair<Lightray, double> >, all the light rays of the
 * cone with corresponding coefficient to be applied to their intensity.
 */
	vector< pair<Lightray, double> > scatter_bounce
(Lightray main_ray_ray, Hit hit, double cone_opening, int nbr_rays,
 int nbr_circles)
{
	assert(cone_opening > Shape::PRECISION);
	assert(cone_opening <= 1);

	//Initialising the vector
	vector< pair<Lightray, double> > rays;

	//creating the vector u, extractiing datas and  getting some relevant
	//norms
	Vector3 main_ray = normalize(main_ray_ray.get_direction());
	Point3 origin = main_ray_ray.get_origin();
	Vector3 normal = hit.get_normal();
	double norm_main_ray = 1.0;
	Vector3 u = get_an_orthogonal_vect(main_ray, normal);

	//initialise the radius of the  cones, length_modif will be updated
	double base_length_modif = (cone_opening * norm_main_ray);
	double length_modif = base_length_modif;

	//creating the base modification vector and the rotation angle
	Vector3 modif_unit = normalize(u);
	double alpha = tan((double)360/(double)nbr_rays);

	//creating the pair we'll use and adding the main_ray to the vector
	// The main ray has a coefficient of 1
	pair <Lightray, double> iter_pair(main_ray_ray, 1.0);
	rays.push_back(iter_pair);

	//Initialising the sum of the coefficients
	double sum_coeff = 0;

	for(int k=1; k<nbr_circles+1; k++)
	{
		// Changes the length of the modification vector
		length_modif =
			update_length_modif(base_length_modif, k, nbr_circles);
		Vector3 modificator = length_modif * modif_unit;

		for(int i=0; i<nbr_rays; i++)
		{
			//uses the modification vector to add a new ray to the
			//vector also updates the sum of the coefficients
			Lightray new_ray
				(origin, normalize(main_ray + modificator));
			iter_pair.first = new_ray;
			iter_pair.second =
				power_scattered_ray(length_modif/norm_main_ray);
			sum_coeff = sum_coeff + iter_pair.second;
			rays.push_back(iter_pair);

			// rotates the modification vector
			Vector3 temp =
				(1/norm_main_ray)* cross(main_ray, modificator);
			Vector3 temp2 = alpha * temp;
			modificator = modificator + temp2;
			modificator = length_modif * normalize(modificator);
		}
	}

	normalize_coeffs(rays, sum_coeff);

	return(rays);

}
