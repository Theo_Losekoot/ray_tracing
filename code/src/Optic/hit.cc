#include "hit.hh"

using namespace std;

/**
 * No hit default value.
 */
const Hit Hit::No_hit(Point3(DBL_MAX, 0, 0), Vector3(0, 0, 0));

/**
 * Create a hit point.
 * 
 * @param Point3 position, the hit position.
 * @param Vector3 normal, normal 3D vector of the impact point.
 */
Hit::Hit(Point3 position, Vector3 normal)
	: position(position), normal(normal)
{
	//clog << "Hit detected at position " << position
	//	<< " with normal vector " << normal << endl;
}

/**
 * Get the hit position.
 * 
 * @return Point3, hit position.
 */
Point3 Hit::get_position() const
{
	return position;
}

/**
 * Get the hit normal.
 * 
 * @return Vector3, hit normal.
 */
Vector3 Hit::get_normal() const
{
	return normal;
}

/**
 * Test if the hit correspond to a hit or to a no hit point.
 * 
 * @return bool, true if the point is hit, false else.
 */
bool Hit::is_hit()
{
	return position.get_x() != DBL_MAX;
}
