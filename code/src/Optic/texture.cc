#include "texture.hh"

using namespace std;

/**
 * Collect all color produce by accessible light sources and their
 * coefficient.
 * 
 * @param Hit impact, impact point.
 * @param vector<Shape*> shapes, container containing all shape pointers
 * used to find sources.
 * 
 * @return vector<pair<Color, double>>, all the couple composed of light
 * rays and their coefficients.
 */

vector<pair<Color, double> > colors_received_by_all_sources
(Hit impact_hit, vector<Shape*> shapes)
{
	vector<pair <Color, double> > colors;

	vector<Lightray> rays =
		compute_lightray_to_source (shapes, impact_hit.get_position());

	for (Lightray ray : rays)
	{
		double coefficient =
			cosinus(impact_hit.get_normal(), ray.get_direction());
		Shape* closest_shape = first_shape_hit(ray, shapes);

		Color shape_color = closest_shape->get_color();

		pair<Color, double> color_coeff (shape_color, coefficient);
		colors.push_back(color_coeff);
	}

	return colors;
}

/**
 * Compute the reflexion color.
 * 
 * @param Color rebound_color, color of the rebound ray.
 * @param double coefficient, coefficient to apply.
 * 
 * @return Color, the reflexion color.
 */
Color compute_reflexion (Color rebound_color, double coefficient) {
	Color color =
		rebound_color * coefficient;

	return color;
}

/**
 * Compute the diffusion color by adding up colors times their 
 * coefficient.
 * 
 * @param vector<pair<Color, double> > sources_color, all the couple of
 * colors and their coefficient to consider.
 * 
 * @return Color, the diffusion color.
 */
	Color compute_diffusion_adding
(vector<pair<Color, double> > sources_color)
{
	Color diffusion_color (0, 0, 0);

	for (pair<Color, double> p : sources_color) {
		Color current_color = p.first * p.second;
		diffusion_color = diffusion_color + current_color;
	}

	return diffusion_color;
}

/**
 * Compute the highlight color.
 * 
 * @param vector<Shape*> shapes, all shape pointers to consider.
 * @param Hit hit_impact, the impact point.
 * @param Point3 view_point, the scene view point.
 * @param coefficient, Phong coefficient used for (H . L)^coefficient.
 * 
 * @return Color, the highlight color.
 */
	Color compute_phong_highlight
(vector<Shape*> shapes, Hit hit_impact, Point3 view_point,
 double coefficient)
{
	Point3 hit_point = hit_impact.get_position();

	vector<Lightray> rays =
		compute_lightray_to_source(shapes, hit_point);

	Color highlight (0, 0, 0);

	for (Lightray ray : rays)
	{
		Vector3 L = ray.get_direction();
		Vector3 V = view_point - hit_point;
		Vector3 H = L + V;
		H = normalize(H);

		Vector3 N = hit_impact.get_normal();
		N = normalize(N);

		double H_dot_N = dot(H, N);
		double coeff_to_apply = pow(H_dot_N, coefficient);

		Shape* closest_shape = first_shape_hit(ray, shapes);

		Color highlight_part = closest_shape->get_color();
		highlight_part = highlight_part * coeff_to_apply;

		highlight= highlight + highlight_part;
	}

	return highlight;
}

/**
 * Compute the final color of the pixel by applying shape's properties
 * and shape absorption.
 * 
 * @param Shape* shape, pointer to the current shape.
 * @param Color diffusion_color, the diffusion color.
 * @param Color reflexion_color, the reflexion color.
 * @param Color highlight_color, the highlight color.
 * 
 * @return Color, the pixel color according to these paramaters.
 */
	Color mix_color_and_absorb
(Shape* shape,
 Color diffusion_color,
 Color reflexion_color,
 Color highlight_color)
{	
	diffusion_color =
		diffusion_color * shape->get_diffusion_coefficient();

	reflexion_color =
		reflexion_color * shape->get_reflexion_coefficient();

	highlight_color =
		highlight_color * shape->get_highlight_coefficient();

	Color total_color =
		diffusion_color + reflexion_color + highlight_color;

	Color shape_color = shape->get_color();

	Color result = Color::absorption(total_color, shape_color);

	return result;
}

/**
 * Compute the color of the pixel.
 * 
 * @param Shape* current_shape, pointer to the current shape.
 * @param vector<Shape*> shapes, all shape pointers to consider.
 * @param Hit hit, the hit point.
 * @param Point3 view_point, the camera position.
 * @param Color rebound_color, the color of the rebound.
 * @param double rebound_coefficient, the coefficient to apply to
 * rebound's color.
 * @param double phong_coefficient, the coefficient of the phong
 * highlight function.
 * 
 * @return Color, the pixel color.
 */
	Color compute_color
(Shape* current_shape, vector<Shape*> shapes, Hit hit,
 Point3 view_point, Color rebound_color, double rebound_coefficient,
 double phong_coefficient) 
{
	vector<pair<Color, double> > sources_color = 
		colors_received_by_all_sources(hit, shapes);

	Color reflexion_color = 
		compute_reflexion(rebound_color, rebound_coefficient);

	Color diffusion_color = 
		compute_diffusion_adding(sources_color);

	Color phong_highlight =
		compute_phong_highlight
		(shapes, hit, view_point, phong_coefficient);

	Color result =
		mix_color_and_absorb (current_shape, diffusion_color, 
				reflexion_color, phong_highlight);

	return result;
}

