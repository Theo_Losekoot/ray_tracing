#include "lightray.hh"

/**
 * Create a light ray.
 * 
 * @param Point3 origin, origin of the light ray.
 * @param Vector3 direction, direction of the light ray.
 */
Lightray::Lightray(Point3 origin, Vector3 direction)
	: origin(origin), direction(direction)
{
	assert(direction.length() > 0);
	// clog << "Creating light ray, origin = " << origin
	//      <<  ", direction = " << direction << endl;
}

/**
 * Get the light ray origin.
 * 
 * @return Point3, light ray origin.
 */
Point3 Lightray::get_origin() const
{
	return origin;
}

/**
 * Get the light ray direction.
 * 
 * @return Vector3, light ray direction.
 */
Vector3 Lightray::get_direction() const
{
	return direction;
}
