#include "optic.hh"

using namespace std;

Point3 view_point (0, 0, 100);
double phong_coefficient = 5.0;

/**
 * Compute the first shape (in distance) hit by the light ray.
 *
 * @param Lightray ray, current light ray.
 * @param vector<Shape*> shapes, shapes to consider.
 *
 * @return Shape*, pointer on the first shape hit.
 */
Shape* first_shape_hit (Lightray ray, vector<Shape*> shapes)
{
	Point3 origin = ray.get_origin();
	Shape* closest_shape = NULL;
	double distance = DBL_MAX;

	for (Shape* p: shapes)
	{
		Hit intersection = p->check_intersection(ray);

		if(intersection.is_hit())
		{

			double distance_hit =
				squared_distance(origin,
					 	 intersection.get_position());

			if(distance > distance_hit)
			{
				distance = distance_hit;
				closest_shape = p;
			}
		}
	}

	return closest_shape;
}

/**
 * Compute a light ray pointing at a shape/
 *
 * @param Point3 origin, origin of the light ray.
 * @param Shape* shape, shape to point at.
 *
 * @return Lightray, the wanted lightray.
 */
Lightray compute_ray_to_reach_shape (Point3 origin, Shape* shape)
{
	Point3 shape_position = shape->get_position();
	Vector3 ray_direction = shape_position - origin;
	Lightray ray (origin, ray_direction);
	return ray;
}

	vector<Lightray> compute_lightray_to_source
(vector<Shape*> shapes, Point3 origin)
{
	vector<Lightray> rays;

	for (Shape* shape : shapes)
	{
		if (shape->is_source())
		{
			Lightray ray= compute_ray_to_reach_shape(origin, shape);
			Shape* closest_shape = first_shape_hit(ray, shapes);

			if (closest_shape == shape)
			{
				rays.push_back(ray);
			}
		}
	}

	return rays;
}

/**
 * Compute a light ray according to Descartes's laws.
 *
 * @param Hit hit, impact point.
 * @param Lightray incoming_ray, the incoming light ray.
 *
 * @return Lightray, the resulted light ray.
 */
Lightray descartes_ray_rebound(Hit hit, Lightray incoming_ray)
{

	Point3 new_begining = hit.get_position();
	Vector3 normal = hit.get_normal();

	Vector3 incoming = incoming_ray.get_direction();

	Vector3 projection =
		dot(incoming, normal) * normal / dot(normal, normal);

	Vector3 new_exit_vect = incoming + ((-2) * projection);
	new_exit_vect = normalize(new_exit_vect);

	return(Lightray(new_begining,new_exit_vect));
}

/**
 * Main function, compute the rebound of the reflexion ray.
 *
 * @param int step, number maximal of steps possibles.
 * @param vector<Shape*> shapes, all the shapes to consider.
 * @param Lightray ray, current light ray.
 *
 * @return Color, the color of the current light ray.
 */
	Color simple_rebound
(int step, vector<Shape*> shapes, const Lightray ray,
 bool is_first_step)
{

	if (step >= 0)
	{
		Shape* closest_shape = first_shape_hit(ray, shapes);

		if (closest_shape != NULL)
		{

			if (!closest_shape->is_source())
			{
				// Compute Descart ray
				Hit hit= closest_shape->check_intersection(ray);
				Lightray next_ray =
				 	descartes_ray_rebound(hit, ray);

				// Compute rebound color
				Color rebound_color =
					simple_rebound(step-1,
						 	shapes, next_ray,
							false);

				// Compute Coefficient
				Vector3 reference = hit.get_normal();

				vector<pair<Color, double> > sources_color =
					colors_received_by_all_sources(hit,
									shapes);

				double rebound_coefficient =
					cosinus (reference,
						 next_ray.get_direction());

				Color result = compute_color(closest_shape,
					shapes,	hit, view_point, rebound_color,
					rebound_coefficient, phong_coefficient);

				return result;
			}
			else if (is_first_step) {
				return closest_shape->get_color();
			}
		}

	}
	return Color::Black;
}

/**
 * Auxiliary function for scatter_rebound, handle the recursive call for
 * every way in the cone handle color merging.
 *
 * @param int step, number maximal of steps possibles.
 * @param Lightray ray, emerging ray.
 * @param Hit hit, impact point (sources of the emerging ray).
 *
 * @return Color, resulted color.
 */
	Color scattering_rebound
(int step, vector<Shape*> shapes, const Lightray ray, Hit hit)
{
	vector< pair<Lightray, double> > cone_rays =
		scatter_bounce(ray, hit, CONE_OPENING, NBR_RAYS, NBR_CIRCLES);

	double sum_red = 0;
	double sum_green = 0;
	double sum_blue = 0;

	for(pair<Lightray, double> iter : cone_rays){
		Color color = scatter_rebound(step, shapes, iter.first, false);

		double coeff = iter.second;

		sum_red = sum_red + (coeff * color.get_red());
		sum_green = sum_green + (coeff * color.get_green());
		sum_blue = sum_blue + (coeff * color.get_blue());
	}

	sum_red = min(sum_red, 255.0);
	sum_green = min(sum_green, 255.0);
	sum_blue = min(sum_blue, 255.0);

	return(Color (sum_red, sum_green, sum_blue));
}

/**
 * Main function, equivalent to simple rebond but with cone.
 *
 * @param int step, number maximal of steps possibles.
 * @param vector<Shape*> shapes, shapes to consider.
 * @param Lightray ray, current light ray.
 *
 * @return Color, the color of the current light ray.
 */
	Color scatter_rebound
(int step, vector<Shape*> shapes, const Lightray ray,
 bool is_first_step)
{
	if (step >= 0)
	{
		Shape* closest_shape = first_shape_hit(ray, shapes);

		if (closest_shape != NULL)
		{

			if (!closest_shape->is_source())
			{
				// Compute Descart ray
				Hit hit= closest_shape->check_intersection(ray);
				Lightray next_ray =
					descartes_ray_rebound(hit, ray);

				// Compute Coefficient
				Vector3 reference = hit.get_normal();

				double rebound_coefficient =
					cosinus(reference,
					 	next_ray.get_direction());

				// Color operations
				Color rebound_color =
					scattering_rebound(step-1, shapes,
						 	   next_ray, hit);

				Color result = compute_color(closest_shape,
					shapes, hit, view_point, rebound_color,
					rebound_coefficient, phong_coefficient);

				return result;
			}
			else if (is_first_step)
			{
				return closest_shape->get_color();
			}
		}

	}
	return Color::Black;
}
