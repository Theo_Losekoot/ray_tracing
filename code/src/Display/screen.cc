#include "screen.hh"

using namespace std;

/**
 * Create a Screen.
 * 
 * @param Point3 c, origin of the screen.
 * @param Point3 p, position of the screen.
 * @param unsigned int w, its width.
 * @param unsigned int h, its height.
 */
Screen::Screen(Point3 c, Point3 p, unsigned w, unsigned h)
	: origin(c), position(p), width(w), height(h)
{
	clog << "Camera created at Point " << origin << endl;
	clog << "Screen created at Point " << position
		<< ", width = " << width << ", height = " << h << endl;
	display.init(width, height, "What nice colors!");
}

/**
 * Get the screen origin.
 * 
 * @return Point3, the screen origin.
 */
Point3 Screen::get_origin()
{
	return origin;
}

/**
 * Set the color of the pixel whose coordinates are (x, y).
 * 
 * @param unsigned int x, the X coordinate of the pixel.
 * @param unsigned int y, the Y coordinate of the pixel.
 * @param Color c, color to set to the pixel.
 */
void Screen::set_pixel(unsigned x, unsigned y, const Color& c)
{
	display.set_pixel(x, y, c);
}

/**
 * Show the screen's render.
 */
void Screen::show()
{

	display.render();
	//display.wait_quit_event();
	display.wait_event_while_displaying();
}

/**
 * Create a light ray starting from the screen's pixel (x, y) to the
 * 3D scene.
 * 
 * @param unsigned int x, the X coordinate of the starting pixel.
 * @param unsigned int y, the Y coordinate of the starting pixel.
 * 
 * @return Lightray, the corresponding light ray.
 */
Lightray Screen::make_ray(unsigned x, unsigned y)
{
	// For the sake of simplicity, the screen is in the plane (x,y)
	Vector3 v(x, y, 0);
	Vector3 direction = (position + v) - origin;
	direction = normalize(direction);
	return Lightray(origin, direction);
}
