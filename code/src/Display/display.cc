#include "display.hh"

using namespace std;

/**
 * Create a display.
 */
Display::Display() {
	clog << "Creating SFML Display" << endl;
}

/**
 * Initialize the display.
 *
 * @param unsigned int width, the display width.
 * @param unsigned int height, the display height.
 * @param string windo_title, the window's title.
 */
void Display::init(unsigned width, unsigned height,
		const std::string& window_title)
{
	window_width = width;
	window.create(sf::VideoMode(width, height), window_title);
	window_texture.create(width, height);
	window_sprite.setTexture(window_texture);
	// https://en.cppreference.com/w/cpp/container/vector/resize
	pixels.resize(width * height * 4);
	for (unsigned x = 0; x < width; ++x)
		for (unsigned y = 0; y < height; ++y)
			set_pixel(x, y, Color::Black); // For instance...
}

/**
 * Set the color of the pixel whose coordinates are (x, y).
 *
 * @param unsigned int x, the X coordinate of the pixel.
 * @param unsigned int y, the Y coordinate of the pixel.
 * @param Color c, color to set to the pixel.
 */
void Display::set_pixel(unsigned x, unsigned y, const Color& c)
{
	// clog << "Painting pixel: " << x << " " << y << endl;
	pixels.at((x + y * window_width) * 4 + 0) = c.get_red();
	pixels.at((x + y * window_width) * 4 + 1) = c.get_green();
	pixels.at((x + y * window_width) * 4 + 2) = c.get_blue();
	pixels.at((x + y * window_width) * 4 + 3) = 255;
}

/**
 * Show display contents, render pixel's colors.
 */
void Display::render()
{
	// https://en.cppreference.com/w/cpp/container/vector/data
	window_texture.update(pixels.data());
	window.draw(window_sprite);
	window.display();
}

/**
 * Quit event main loop.
 */
void Display::wait_quit_event()
{
	while (window.isOpen())
	{
		sf::Event event;
		// https://www.sfml-dev.org/documentation/2.5.1-fr/classsf_1_1Window.php
		// Could use the pollEvent function if needed,
		// but polling uses busy waiting, which is expensive
		while (window.waitEvent(event))
		{
			switch (event.type)
			{
				case sf::Event::Closed:
					window.close();
					break;
				default:
					break;
			}
		}
	}
}

/**
 * Displaying event main loop.
 */
void Display::wait_event_while_displaying()
{
	while (window.isOpen())
	{
		sf::Event event;
		// https://www.sfml-dev.org/documentation/2.5.1-fr/classsf_1_1Window.php
		// Could use the pollEvent function if needed,
		// but polling uses busy waiting, which is expensive
		while (window.pollEvent(event))
		{
			std::this_thread::sleep_for(std::chrono::milliseconds(10));
			this->render();
			if(event.type == sf::Event::Closed)
			{
				window.close();
			}
			else if(event.type == sf::Event::KeyPressed)
			{
				switch (event.key.code)
				{
					case sf::Keyboard::Return:
						window.close();
						break;
					default:
						break;
				}
			}

		}
	}
}
