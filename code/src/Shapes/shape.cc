#include "shape.hh"

using namespace std;

/**
 * Container containing all existing shapes.
 */
vector<Shape*> Shape::shapes;

/**
 * Define the shape precision used for double approximation.
 */
const double Shape::PRECISION = 0.0001;

/**
 * Create a shape.
 *
 * @param Point3 p, shape's position.
 * @param Color c, shape's color.
 * @param bool is_source, true if the shape is a light source.
 * @param double diff_coeff, diffusion coefficient.
 * @warning 0 <= diff_coeff <= 1
 * @param double ref_coeff, reflexion coefficient.
 * @warning 0 <= ref_coeff <= 1
 * @param double highlight_coeff, highlight coefficient.
 * @warning 0 <= highlight_coeff <= 1
 */
Shape::Shape(Point3 p, Color color, bool is_source,
		double diff_coeff, double ref_coeff, double highlight_coeff)
	: position(p), color(color), produce_light(is_source),
	diffusion_coefficient(diff_coeff), reflexion_coefficient(ref_coeff),
	highlight_coefficient(highlight_coeff)
{
	assert(0.0 <= diff_coeff && diff_coeff <= 1.0);
	assert(0.0 <= ref_coeff && ref_coeff <= 1.0);
	assert(0.0 <= highlight_coeff && highlight_coeff <= 1.0);

	clog << "Creating a shape at position = " << position << endl;
	record();
}

/**
 * Find the indice of a shape pointer inside a container. Return max
 * unsigned int, if the shape pointer is not found.
 *
 * @param Shape* obj, a shape pointer.
 * @param vector<Shape*> objects, container containing all existing
 * shape pointers.
 *
 * @return unsigned int, the shape pointer indice inside the container.
 */
	unsigned int find_indice_object
(Shape* obj, const vector<Shape*>& objects)
{
	for(unsigned int i=0; i<objects.size(); i++)
	{
		if (obj == objects[i])
			return i;
	}
	return std::numeric_limits<unsigned int>::max();
}

/**
 * Shape destructor.
 */
Shape::~Shape()
{
	unsigned int indice = find_indice_object(this, shapes);

	assert(indice != std::numeric_limits<unsigned int>::max());

	shapes.erase(shapes.begin() + indice);
}

/**
 * Get the shape's color.
 *
 * @return Color, the shape's color.
 */
Color Shape::get_color() const
{
	return color;
}

/**
 * Get the shape's position.
 *
 * @return Point3, the shape's position.
 */
Point3 Shape::get_position() const
{
	return position;
}

/**
 * Get the container containing all existing shape pointor.
 *
 * @return vector<Shape*>, the shape's pointor container.
 */
vector<Shape*> Shape::get_shapes()
{
	return shapes;
}

/**
 * Add a shape pointer to the shape pointer container.
 */
void Shape::record()
{
	shapes.push_back(this);
}

/**
 * Print a shape inside the command console.
 */
void Shape::print() const
{
	/*
	   for(int i=0; i<shapes.size(); i++)
	   {
	   shapes[i]->position = Point3();
	   }
	 */
	clog << "Shape: Position = " << position << endl;
}

/**
 * Test if the current shape is a source or not.
 *
 * @return bool, true if the shape is a light source, false else.
 */
bool Shape::is_source() const
{
	return produce_light;
}

/**
 * Get the diffusion coefficient of the current shape.
 *
 * @return double, the diffusion coefficient.
 */
double Shape::get_diffusion_coefficient() const {
	return diffusion_coefficient;
}

/**
 * Get the reflexion coefficient of the current shape.
 *
 * @return double, the reflexion coefficient.
 */
double Shape::get_reflexion_coefficient() const {
	return reflexion_coefficient;
}

/**
 * Get the highlight coefficient of the current shape.
 *
 * @return double, the highlight coefficient.
 */
double Shape::get_highlight_coefficient() const {
	return highlight_coefficient;
}
