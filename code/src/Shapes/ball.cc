#include <iostream>
#include <cmath>
#include <cassert>
#include "color.hh"
#include "ball.hh"
#include "point3.hh"

using namespace std;

/**
 * Create a ball.
 *
 * @param Point3 center, ball's center.
 * @param double radius, ball's radius.
 * @param Color c, ball's color.
 * @param bool is_source, true if the ball is a light source.
 * @param double diff_coeff, diffusion coefficient.
 * @warning 0 <= diff_coeff <= 1
 * @param double ref_coeff, reflexion coefficient.
 * @warning 0 <= ref_coeff <= 1
 * @param double highlight_coeff, highlight coefficient.
 * @warning 0 <= highlight_coeff <= 1
 */
Ball::Ball
(Point3 center, double radius, Color c, bool is_source,
 double diff_coeff, double ref_coeff, double highlight_coeff):
	Shape(center, c, is_source, diff_coeff, ref_coeff, highlight_coeff),
	radius(radius)
{
	//record (); // Watch it!
	clog << "Creating a ball, center = " << center
		<< ", radius = " << radius << endl;
}

/**
 * Compute the smallest positive value between two.
 *
 * @param double d1, first value.
 * @param double d2, second value.
 *
 * @return double, smallest positive value between d1 and d2.
 */
double positive_min (double d1, double d2) {
	if (d1 < d2 && 0 <= d1) {
		return d1;
	}
	return d2;
}

/**
 * Check the intersection between a light ray and a ball.
 *
 * @param Lightray ray, a light ray.
 *
 * @return Hit, compute the hit point between a light ray and the shape,
 * return No_hit if no hit point is found.
 */
Hit Ball::check_intersection(Lightray ray) const
{
	Point3 ray_origin = ray.get_origin();
	Vector3 ray_direction = ray.get_direction();

	Vector3 AC = position - ray_origin;

	double A = dot(ray_direction, ray_direction);
	double B = - 2 * dot (AC, ray_direction);
	double C = dot(AC, AC) - radius * radius;

	double delta = B * B - 4 * A * C;

	if (delta > Shape::PRECISION)
	{
		// Roots
		double d1 = (- B - sqrt(delta)) / (2 * A);
		double d2 = (- B + sqrt(delta)) / (2 * A);

		if(d1 < Shape::PRECISION && d2 < Shape::PRECISION){
			return Hit::No_hit;
		}

		double best_root = positive_min(d1, d2);

		// Interception point
		Point3 I = ray_origin + (best_root * ray_direction);

		Vector3 OI = I - position;
		double norm_OI = dot(OI, OI);

		if (norm_OI <= radius * radius) {
			// Move I outside the circle
			Vector3 vect_unit = normalize(OI);
			Point3 I_prime = I+((2 * Shape::PRECISION) * vect_unit);
			OI = I_prime - position;
		}

		norm_OI = dot(OI, OI);

		assert(dot(OI, OI) >= radius*radius);

		OI = normalize(OI);

		assert(
				dot(OI, OI) < 1 + Shape::PRECISION
				&& dot(OI, OI) > 1 - Shape::PRECISION
			  );

		return Hit (I, OI);
	}
	return Hit::No_hit;
}
