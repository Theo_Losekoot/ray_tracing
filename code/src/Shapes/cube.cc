#include "cube.hh"

using namespace std;

/**
 * Create a cube.
 *
 * @param Point3 center, cube's center.
 * @param Vector3 width, a 3D vector defining the cube whose length are
 * the plane width.
 * @param Vector3 height, a 3D vector defining the cube whose length
 * are the plane height.
 * @param Vector3 depth, a 3D vector defining the cube whose length
 * are the plane depth.
 * @param Color c, cube's color.
 * @param bool is_source, true if the cube is a light source.
 * @param double diff_coeff, diffusion coefficient.
 * @warning 0 <= diff_coeff <= 1
 * @param double ref_coeff, reflexion coefficient.
 * @warning 0 <= ref_coeff <= 1
 * @param double highlight_coeff, highlight coefficient.
 * @warning 0 <= highlight_coeff <= 1
 */
	Cube::Cube
(Point3 center, Vector3 width, Vector3 height, Vector3 depth,
 Color color, bool is_source, double diff_coeff, double ref_coeff,
 double highlight_coeff)
	: Shape(center, color, is_source, diff_coeff, ref_coeff,
		highlight_coeff), size_x(width), size_y(height), size_z(depth)
{
	clog << "Creating a cube, position = " << position
		<< ", depth  = " << depth
		<< ", width = " << width << ", height = " << height << endl;

	//CREATE 6 SIDES

	Plane* side_x_1 = new Plane
		(position -size_x/2, size_y, size_z,
		 color, is_source, diff_coeff, ref_coeff, highlight_coeff);
	Plane* side_x_2 = new Plane
		(position +size_x/2, size_y, size_z,
		 color, is_source, diff_coeff, ref_coeff, highlight_coeff);
	Plane* side_y_1 = new Plane
		(position -size_y/2, size_z, size_x,
		 color, is_source, diff_coeff, ref_coeff, highlight_coeff);
	Plane* side_y_2 = new Plane
		(position +size_y/2, size_z, size_x,
		 color, is_source, diff_coeff, ref_coeff, highlight_coeff);
	Plane* side_z_1 = new Plane
		(position -size_z/2, size_x, size_y,
		 color, is_source, diff_coeff, ref_coeff, highlight_coeff);
	Plane* side_z_2 = new Plane
		(position +size_z/2, size_x, size_y,
		 color, is_source, diff_coeff, ref_coeff, highlight_coeff);

	planes.push_back(side_x_1);
	planes.push_back(side_x_2);
	planes.push_back(side_y_1);
	planes.push_back(side_y_2);
	planes.push_back(side_z_1);
	planes.push_back(side_z_2);
}

/**
 * Cube destructor.
 */
Cube::~Cube()
{
	for(Plane* side : planes)
	{
		delete side;
	}
}

/**
 * Check the intersection between a light ray and a cube.
 *
 * @param Lightray ray, a light ray.
 *
 * @return Hit, compute the hit point between a light ray and the shape,
 * return No_hit if no hit point is found.
 */
Hit Cube::check_intersection(Lightray ray) const
{
	/* Nothing need to be done, because each plane exist inside
	 * the shape pointers container and plane check_intersection
	 * already exist. */

	// Line used to remove unused-variable warning.
	ray = ray;

	return Hit::No_hit;
}
