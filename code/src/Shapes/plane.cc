#include "plane.hh"

using namespace std;

/**
 * Compute the position of the bottom left corner of the plane.
 *
 * @param Point3 position, plane position.
 * @param Vector3 width, plane width.
 * @param Vector3 height, plane height.
 * @warning 3D vectors width and height define the plane.
 *
 * @return Point3, the bottom left corner point of the plane.
 */
	Point3 get_bottom_left_corner
(Point3 position, Vector3 width, Vector3 height)
{
	Point3 new_position = position - width/2 - height/2;
	return new_position;
}

/**
 * Create a plane.
 *
 * @param Point3 position, plane's center.
 * @param Vector3 width, a 3D vector defining the plane whose length are
 * the plane width.
 * @param Vector3 height, a 3D vector defining the plane whose length
 * are the plane height.
 * @param Color c, plane's color.
 * @param bool is_source, true if the plane is a light source.
 * @param double diff_coeff, diffusion coefficient.
 * @warning 0 <= diff_coeff <= 1
 * @param double ref_coeff, reflexion coefficient.
 * @warning 0 <= ref_coeff <= 1
 * @param double highlight_coeff, highlight coefficient.
 * @warning 0 <= highlight_coeff <= 1
 */
	Plane::Plane
(Point3 position, Vector3 width, Vector3 height, Color color,
 bool is_source, double diff_coeff, double ref_coeff,
 double highlight_coeff)
	: Shape(get_bottom_left_corner(position, width, height),
		color, is_source, diff_coeff, ref_coeff, highlight_coeff),
		width(width), height(height)
{
	normal = normalize(cross(width, height));

	clog << "Creating a plane, position = " << position
		<< ", normal = " << normal
		<< ", width = " << width << ", height = " << height << endl;
}

/**
 * Test the presence of a point inside a rectange.
 *
 * @param Point3 origin, the plane's origin.
 * @param Point3 to_test, 3D points to test the presence inside the
 * wanted rectange.
 * @param Vector3 height_vect, 3D vector defining the rectangle plane
 * whose length is the rectangle height.
 * @param Vector3 width_vect, 3D vector defining the rectangle plane
 * whose length is the rectangle width.
 *
 * @return bool, true if the point to_test is inside the rectangle,
 * false else.
 */
	bool in_rectangle
(Point3 origin,
 Point3 to_test,
 Vector3 height_vect,
 Vector3 width_vect)
{
	Vector3 vect_pos = to_test - origin;

	double height_vect_norme = dot(height_vect, height_vect);
	double width_vect_norme = dot(width_vect, width_vect);

	double scal1 = dot(vect_pos,height_vect);
	double scal2 = dot(vect_pos,width_vect);

	bool condition1 = scal1 > 0 && scal1 < height_vect_norme;
	bool condition2 = scal2 > 0 && scal2 < width_vect_norme;

	return(condition1 && condition2);
}

/**
 * Check the intersection between a light ray and a plane.
 *
 * @param Lightray ray, a light ray.
 *
 * @return Hit, compute the hit point between a light ray and the shape,
 * return No_hit if no hit point is found.
 */
Hit Plane::check_intersection(Lightray ray) const
{
	Vector3 ray_direction = ray.get_direction();
	Point3 ray_origin = ray.get_origin();

	// need to solve : (ray_direction * t + ray_origin - p) . normal = 0

	double parallel = dot(ray_direction, normal);


	if (parallel > Shape::PRECISION || parallel < -1 * Shape::PRECISION)
	{
		double solution_part1 = dot((position - ray_origin), normal);

		if(abs(solution_part1) < Shape::PRECISION)
		{
			return Hit::No_hit;
		}
		double d = solution_part1 / parallel;

		if (d > Shape::PRECISION) {
			Point3 solution =  ray_origin + (d * ray_direction);
			bool is_in_rectangle =
				in_rectangle(position, solution, height, width);

			if (is_in_rectangle)
			{
				return Hit(solution, normal);
			}
		}
	}

	return Hit::No_hit;
}
