#pragma once

#include <iostream>

#include "point3.hh"
#include "lightray.hh"
#include "vector3.hh"
#include "display.hh"

class Screen
{
	private:
		Point3 origin;
		Point3 position;
		unsigned width;
		unsigned height;
		Display display;
	public:
		Screen(Point3 c, Point3 p, unsigned w, unsigned h);
		Point3 get_origin();
		void set_pixel(unsigned x, unsigned y, const Color& c);
		void show();
		Lightray make_ray(unsigned x, unsigned y);
};
