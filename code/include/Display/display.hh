#pragma once

#include <string>
#include <SFML/Graphics.hpp>
#include <vector>
#include <iostream>
#include <chrono>
#include <thread>

#include "color.hh"

class Display
{
	public:
		Display();
		void init(unsigned width, unsigned height,
				const std::string& window_title);

		void set_pixel(unsigned x, unsigned y, const Color& c);
		void render();
		void wait_quit_event();
		void wait_event_while_displaying();
	private:

		sf::RenderWindow window;
		unsigned window_width;
		sf::Texture window_texture;
		sf::Sprite window_sprite;
		std::vector<sf::Uint8> pixels;
};
