#include <vector>
#include <cmath>

#include "point3.hh"
#include "vector3.hh"
#include "shape.hh"
#include "hit.hh"
#include "lightray.hh"
#include "optic.hh"

std::vector<std::pair<Color, double> > colors_received_by_all_sources
	(Hit impact_hit, std::vector<Shape*> shapes);

Color compute_color
	(Shape* current_shape, std::vector<Shape*> shapes,
	Hit hit, Point3 view_point, Color rebound_color, 
	double rebound_coefficient, double phong_coefficient);
