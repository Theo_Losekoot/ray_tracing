#pragma once

#include <iostream>
#include <vector>
#include <cmath>
#include <cfloat>

#include "point3.hh"
#include "vector3.hh"
#include "shape.hh"
#include "hit.hh"
#include "lightray.hh"
#include "scatter.hh"
#include "texture.hh"

#define CONE_OPENING 0.7
#define NBR_RAYS 3
#define NBR_CIRCLES 1

std::vector<Lightray> compute_lightray_to_source 
	(std::vector<Shape*> shapes, Point3 origin);

Shape* first_shape_hit (Lightray ray, std::vector<Shape*> shapes);

Color simple_rebound 
	(int step, std::vector<Shape*> shapes, const Lightray ray, 
	bool is_first_step = true);

Color scatter_rebound
	(int step, std::vector<Shape*> shapes, const Lightray ray, 
	bool is_first_step = true);
