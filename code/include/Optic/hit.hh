#pragma once

#include <cfloat>
#include <iostream>

#include "point3.hh"
#include "vector3.hh"

class Hit
{
	private:
		Point3 position;
		Vector3 normal;
	public:
		static const Hit No_hit;
		Hit(Point3 position, Vector3 normal);
		Point3 get_position() const;
		Vector3 get_normal() const;
		bool is_hit();
};
