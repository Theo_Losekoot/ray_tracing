#pragma once

#include <cassert>
#include <iostream>

#include "point3.hh"
#include "vector3.hh"

class Lightray
{
	private:
		Point3 origin;
		Vector3 direction;
	public:
		Lightray(Point3 origin, Vector3 direction);
		Point3 get_origin() const;
		Vector3 get_direction() const;
};
