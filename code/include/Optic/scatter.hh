#pragma once
#include <unordered_map>
#include <cmath>
#include <cassert>
#include <iostream>
#include <utility>
#include <vector>

#include "point3.hh"
#include "vector3.hh"
#include "shape.hh"
#include "hit.hh"
#include "lightray.hh"

std::vector< std::pair<Lightray, double> > scatter_bounce
	(Lightray main_ray_ray, Hit hit, double coeff, int nbr_rays, 
	int nbr_circles);
