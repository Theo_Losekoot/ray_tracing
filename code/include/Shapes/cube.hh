#pragma once

#include <vector>

#include "shape.hh"
#include "hit.hh"
#include "lightray.hh"
#include "color.hh"
#include "plane.hh"
#include "point3.hh"

class Cube : public Shape
{
	private:
		Vector3 size_x;
		Vector3 size_y;
		Vector3 size_z;
		std::vector<Plane*> planes;
	public:
		Cube(Point3 center, Vector3 width, Vector3 height,
			 Vector3 depth, Color color=Color(),
			 bool is_source = false, double diff_coeff = 0.8,
			 double ref_coeff = 0.3, double highlight_coeff = 0.2);
			 
		virtual ~Cube();
		virtual Hit check_intersection(Lightray ray) const override;
};
