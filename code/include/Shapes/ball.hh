#pragma once
#include "shape.hh"
#include "hit.hh"
#include "lightray.hh"
#include "color.hh"

class Ball : public Shape
{
	private:
		double radius;
	public:
		Ball(Point3 center, double radius, Color c = Color(),
			bool is_source = false, double diff_coeff = 0.8,
			double ref_coeff = 0.3, double highlight_coeff = 0.2);

		virtual Hit check_intersection(Lightray ray) const override;
};
