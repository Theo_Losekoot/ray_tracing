#pragma once
#include <vector>
#include <iostream>
#include <cassert>
#include <limits>

#include "hit.hh"
#include "lightray.hh"
#include "color.hh"

class Shape
{
	protected:
		Point3 position;
		Color color;
		static std::vector<Shape*> shapes;
		bool produce_light;

		double diffusion_coefficient;
		double reflexion_coefficient;
		double highlight_coefficient;

	public:
		static const double PRECISION;
		Shape(Point3 p, Color color = Color(), bool is_source = false,
			double diff_coeff = 1.0, double ref_coeff = 1.0,
			double highlight_coeff = 0.2);

		virtual ~Shape();
		Color get_color() const;
		Point3 get_position() const;
		static std::vector<Shape*> get_shapes();
		void record();
		void print() const;
		virtual Hit check_intersection(Lightray ray) const = 0;
		bool is_source() const;

		double get_diffusion_coefficient() const;
		double get_reflexion_coefficient() const;
		double get_highlight_coefficient() const;
};
