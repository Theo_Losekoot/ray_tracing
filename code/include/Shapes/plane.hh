#pragma once

#include <iostream>
#include <stdlib.h>

#include "shape.hh"
#include "hit.hh"
#include "lightray.hh"
#include "color.hh"


class Plane : public Shape
{
	private:
		Vector3 normal;
		Vector3 width;
		Vector3 height;
	public:
		Plane(Point3 position, Vector3 width, Vector3 height,
			Color color = Color(), bool is_source = false,
			double diff_coeff = 0.8, double ref_coeff = 0.3,
			double highlight_coeff = 0.2);
			
		virtual Hit check_intersection(Lightray ray) const override;
};
