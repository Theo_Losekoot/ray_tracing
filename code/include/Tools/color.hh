/* Copyright (c) 2017-2018 Jean-Michel Gorius */
#pragma once

#include <SFML/Graphics.hpp>
#include <cassert>

class Color
{
	public:

		static Color White;
		static Color Black;
		static Color Red;
		static Color Green;
		static Color Blue;
		static Color Yellow;
		static Color Magenta;
		static Color Cyan;
		static Color Grey;

		Color() = default;
		Color(int red, int green, int blue);
		Color(const Color& other) = default;
		Color& operator =(const Color& rhs) = default;

		sf::Color to_SFMLColor() const;

		int get_red() const;
		int get_green() const;
		int get_blue() const;

		Color operator +(const Color& c) const;
		Color operator *(const double d) const;
		Color operator /(const double d) const;
		bool operator ==(const Color& c) const;
		static Color absorption(const Color& c1, const Color& c2);

	private:

		int red;
		int green;
		int blue;
};

Color mix_colors(const std::vector< std::pair<Color, double> >& colors);
std::ostream& operator <<(std::ostream& s, const Color& c);
