#pragma once

#include "vector3.hh"

class Point3
{
	public:

		Point3() = default;
		Point3(double x, double y, double z);
		Point3(const Point3& other) = default;
		Point3& operator =(const Point3& rhs) = default;
		~Point3() = default;

		double get_x() const;
		double get_y() const;
		double get_z() const;

	private:

		double x;
		double y;
		double z;
};

Point3 operator +(const Point3& p, const Vector3& v);
Point3 operator -(const Point3& p, const Vector3& v);
Vector3 operator -(const Point3& p1, const Point3& p2);
Point3 operator *(const Point3& p1, const Point3& p2);
double squared_distance(const Point3& p1, const Point3& p2);

std::ostream& operator <<(std::ostream& s, const Point3& p);
