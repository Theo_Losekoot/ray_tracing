#pragma once

#include <iostream>
#include <cmath>
#include <cassert>

class Vector3
{
	public:

		Vector3() = default;
		Vector3(double x, double y, double z);
		Vector3(const Vector3& other) = default;
		~Vector3() = default;
		Vector3& operator =(const Vector3& rhs) = default;
		bool operator ==(const Vector3& v) const;

		double length() const;

		double get_x() const;
		double get_y() const;
		double get_z() const;

	private:

		double x;
		double y;
		double z;
};

Vector3 operator *(double t, const Vector3& v);
Vector3 operator /(const Vector3& v, double t);
Vector3 operator +(const Vector3& u, const Vector3& v);
Vector3 operator -(const Vector3& u, const Vector3& v);
Vector3 operator -(const Vector3& v);

double dot(const Vector3& v1, const Vector3& v2);
Vector3 cross(const Vector3& v1, const Vector3& v2);

Vector3 normalize(const Vector3& v);

std::ostream& operator <<(std::ostream& s, const Vector3& v);

double cosinus (const Vector3& reference, const Vector3& ray);
